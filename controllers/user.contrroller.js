const crypto = require('crypto');
const userModel = require("../model/user");

var sess;
convertMD5 = (params) => {
    return crypto.createHash('md5').update(params).digest("hex");
}

exports.getAll = (req, res) => {
    userModel.find({}, (err, data) => {
        if(err) {
            reponse = {"message": false}
        }else {
            reponse = {"message": data}
        }
        res.send(reponse);
    });
}

exports.login = (req, res) => {
    var username = req.body.username;
    var password = convertMD5(req.body.password);

    userModel.find({username :username, password:password},
        (err, data) => {
            if(err){
                reponse = {"message": false};
            }else{
                if(data == ''){
                    reponse = {"message": false};
                }else{
                    sess=req.session;
                    sess.username = username;
                    reponse = {"message": true,"session":req.session};
                }
            }
            res.send(reponse);
        });
}

exports.logout = (req, res) => {
    if (req.session.has('username')){
        req.session.forget('username');
        reponse = {"message": false};
    }else{
        reponse = {"message": true};
    }
    res.send(reponse);
}

exports.checklogin = (req, res) => {
    if (typeof sess === 'undefined'){
        reponse = {"message": false};
    }else{
        reponse = {"message": true};
    }
    res.send(reponse);
}

exports.userInsert = (req, res) => {
    var data = new userModel();
    data.username = req.body.username;
    data.password = crypto.createHash('md5').update(req.body.password).digest("hex");
    data.groub = req.body.groub;

    data.save((err) => {
        if(err) {
            console.log(err);
            reponse = {"message": false}
        }else {
            console.log("Insert User Successfull");
            reponse = {"message": "OK"}
        }
        res.send(reponse);
    });
}