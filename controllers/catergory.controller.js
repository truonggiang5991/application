const categoryModel = require("../model/category");

exports.category = function (req, res) {
    categoryModel.find({}, (err, data) => {
        if(err) {
            reponse = {"message": false}
        }else {
            reponse = {"message": data}
        }
        res.send(reponse);
    });
};

exports.categoryInsert = (req, res) => {
    var data = new categoryModel();
    data.name = req.body.name;
    data.description = req.body.description;
    data.save((err) => {
        if(err) {
            console.log(err);
            reponse = {"message": false}
        }else {
            console.log("Insert Category Successfull");
            reponse = {"message": "OK"}
        }
        res.send(reponse);
    });
}