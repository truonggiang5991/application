const express = require('express');
const router = express.Router();

const catergory_controller = require('../controllers/catergory.controller');

router.get('/', catergory_controller.category);

router.post('/insert', catergory_controller.categoryInsert);

module.exports = router;