const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.contrroller');

router.get('/', user_controller.getAll);
router.post('/login', user_controller.login);
router.get('/logout', user_controller.logout);
router.get('/checklogin', (user_controller.checklogin));
router.post('/insert', user_controller.userInsert);

module.exports = router;
