const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const NodeSession = require('node-session');
const url = require("./db");
const category = require('./routes/category.route');
const user = require('./routes/user.route');
var cookieParser = require('cookie-parser');
var session = require('express-session');
const nodeSession = new NodeSession({secret: 'Q3UBzdH9GEfiRCTKbi5MTPyChpzXLsTD'});

const app = express();
const port = process.env.PORT || 5000;

// Use connect method to connect to the Servermongoose.Promise = global.Promise;
mongoose.connect(url.url,{useNewUrlParser: true })
.then(() => {
    console.log("Successfully connected to MongoDB.");
}).catch(err => {
    console.log('Could not connect to MongoDB.');
    process.exit();
}); 

// function session(req, res, next){
//     nodeSession.startSession(req, res, next);
// }
app.use(cookieParser());
app.use(session({ 
    secret : 'yourSecret',
    resave : false,
    saveUninitialized : true,
}));

// app.use(session);
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/public')));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, content type, Authorization, Accept');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/category', category);
app.use('/user', user);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
