import React, { Component } from "react";
import axios from 'axios';
import { Redirect, Switch, Route } from 'react-router-dom';
import { Button, ToastContainer, toast } from 'mdbreact'
import $ from 'jquery';
import "../../css/login.css";
import image from "../../image/background.jpg";
import Home from '../home';

function sendResquest(state, callback){
    axios.post('/user/login', {
        username: state.username,
        password: state.password
    })
    .then(res => {
        callback(res.data.message);
    });
}

function validate(state){
    var errorMessage = {
        isInputValid: true,
        username: "",
        password: ""
    }

    if(state.username === ''){
        errorMessage.isInputValid = false;
        errorMessage.username = "Ban chua nhap UserName!!!!!";
    }

    if(state.password === ''){
        errorMessage.isInputValid = false;
        errorMessage.password = "Ban chua nhap PassWord!!!!!";
    }

    return errorMessage;
}

export default class Login extends Component{

    constructor(props){
        super(props);
        
        this.handleChangeUserName = this.handleChangeUserName.bind(this);
        this.handleChangePassWord = this.handleChangePassWord.bind(this);
        this.login = this.login.bind(this);
        this.enterLogin = this.enterLogin.bind(this);

        this.state = {
            username: '',
            password: '',
            message: '',
            redirect: ''
        }
    }
    componentWillMount(){
        axios.get("http://localhost:5000/user/checklogin")
        .then(res => {
            if(res.data.message){
                this.setState({redirect: true});
            }
        });
      }
      renderRedirect = () => {
        if (this.state.redirect === false) {
          return <Redirect to='/login' />
        }
      }
    componentDidMount(){
        $("#password").focusin(function () {
            $("form").addClass("up");
        });
        $("#password").focusout(function () {
            $("form").removeClass("up");
        });

        // Panda Eye move
        $(document).on("mousemove", function (event) {
            var dw = $(document).width() / 15;
            var dh = $(document).height() / 15;
            var x = event.pageX / dw;
            var y = event.pageY / dh;
            $(".eye-ball").css({
                width: x,
                height: y
            });
        });
    }

    handleChangeUserName(e){
        this.setState({username: e.target.value});
    }

    handleChangePassWord(e){
        this.setState({password: e.target.value});
    }

    login(){

        var error = validate(this.state);
        if(!error.isInputValid){
            if(error.username !== ''){
                toast.error(error.username);
            }
            
            if(error.password !== ''){
                toast.error(error.password);
            }
        }else{
            sendResquest(this.state, (response) => {
                if (!response) {
                    this.setState({message: "Dang nhap sai!!!!!"});
                    $("form").addClass("wrong-entry");
                    setTimeout(function () {
                        $("form").removeClass("wrong-entry");
                    }, 3000);
                }else{
                    this.setState({redirect: true});
                }
            });
        }
    }

    enterLogin(e){
        if(e.key === 'Enter'){
            sendResquest(this.state, (response) => {
                if (!response) {
                    this.setState({message: "Dang nhap sai!!!!!"});
                    $("form").addClass("wrong-entry");
                    setTimeout(function () {
                        $("form").removeClass("wrong-entry");
                    }, 3000);
                }else{
                    this.setState({redirect: true});
                }
            });
        }
    }

    render(){
        if(this.state.redirect === true){
            return (
                <Redirect to='/' />
            );
        }else{
            return(
                <div className="login">
                    <div className="image"><img src={image} alt={image}  /></div>
                    <div className="panda">
                        <div className="ear"></div>
                        <div className="face">
                            <div className="eye-shade"></div>
                            <div className="eye-white">
                            <div className="eye-ball"></div>
                            </div>
                            <div className="eye-shade rgt"></div>
                            <div className="eye-white rgt">
                            <div className="eye-ball"></div>
                            </div>
                            <div className="nose"></div>
                            <div className="mouth"></div>
                        </div>
                        <div className="body"> </div>
                        <div className="foot">
                            <div className="finger"></div>
                        </div>
                        <div className="foot rgt">
                            <div className="finger"></div>
                        </div>
                    </div>
                    <form>
                        <div className="hand"></div>
                        <div className="hand rgt"></div>
                        <h1>Panda Login</h1>
                        <div className="form-group">
                            <input onChange={this.handleChangeUserName} required="required" className="form-control"/>
                            <label className="form-label">Username    </label>
                        </div>
                        <div className="form-group">
        
                            <input onChange={this.handleChangePassWord} onKeyDown={this.enterLogin} id="password" type="password" required="required" className="form-control"/>
                            <label className="form-label">Password</label>
                            <p className="alert">{this.state.message}</p>
                            <Button onClick={this.login} type="button" className="btn">Login </Button>
                        </div>
                    </form>
                    <ToastContainer
                        hideProgressBar={true}
                        newestOnTop={true}
                        autoClose={3000}
                    />
                </div>
            );
        }
    }
}