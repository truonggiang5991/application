import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

class Home extends Component {

  constructor(props){
    super(props);

    this.logout = this.logout.bind(this);
  }

  logout(){
    axios.get('/user/logout')
    .then(res => {
      //nothing
    });
  }

  render() {
    return (
      <div>
        <h3>Day la trang home</h3>
        <Link to='/' onClick={this.logout}>Logout</Link>
      </div>
    );
  }
}

  
  export default Home;