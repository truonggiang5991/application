import React, { Component } from 'react';
import axios from 'axios';
import { Redirect , Route, Switch } from 'react-router-dom';
import Login from './vector/login/login';
import Home from './vector/home';
import {browserHistory} from "react-router";

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      redirect: false
    }
  }

  componentWillMount(){
    axios.get("http://localhost:5000/user/checklogin")
    .then(res => {
        if(res.data.message){
            this.setState({redirect: true});
            return <Redirect to='/' /> 
        }
        console.log(this.state.redirect);
        return <Redirect to='/login' />
    });
  }
  renderRedirect = () => {
    if (this.state.redirect === false)
      return <Redirect to='/login' />
    if (this.state.redirect === true) 
      return <Redirect to='/' />
  }
  render() {
    {this.renderRedirect()}
    return (
      <Switch>
          <Route exact path='/' component={Home}/>
          <Route  path='/login' component={Login}/>
      </Switch>
    );
  }
}

export default App;
