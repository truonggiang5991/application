var mongoose = require('mongoose');
var categoryShema = new mongoose.Schema({
    "name": {
        type: String,
        required: true
    },
    "description": String,
    "create_date": {
        type:Date,
        default: Date.now()
    }
},{ versionKey: false });
module.exports = mongoose.model('categories', categoryShema);